/**
 * Jeder neue Term in der Fibonacci-Reihe wird gebildet, 
 * indem die beiden vorherigen Zahlen addiert werden. 
 * Wenn man mit 1 und 2 beginnt, sind die ersten 10 Terme wie folgt:
 * 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
 * Finden Sie die Summe aller geraden Terme der Fibonacci-Reihe, die 4 Millionen nicht überschreiten.
 */


public class ProjektEuler2 {
    public static void main(String [] args){
        int sum = 0;
		int x = 1;  // Represents the current Fibonacci number being processed
		int y = 2;  // Represents the next Fibonacci number in the sequence
		while (x <= 4000000) {
			if (x % 2 == 0)
				sum += x;
			int z = x + y;
			x = y;
			y = z;
		}
        System.out.println(sum);
    }
}
