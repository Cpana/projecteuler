/**
 * 
 *Wenn wir alle natürlichen Zahlen unter 10 auflisten, 
 *die Vielfache von 3 oder 5 sind, so erhalten wir 3, 5, 6 und 9. 
 *Die Summe dieser Vielfachen ist 23.Finden Sie die Summe aller Vielfachen von 3 oder 5 unter 1000.
 *
 */



public class ProjektEuler1{
    public static void main(String[] args) {
        int sum = 0;
		for (int i = 0; i < 1000; i++) {
			if (i % 3 == 0 || i % 5 == 0)
				sum += i;
		}
        System.out.println(sum);
    }
}