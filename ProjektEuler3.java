/**
 * Die Primfaktoren von 13195 sind 5, 7, 13 und 29.
 * Was ist der größte Primfaktor der Zahl 600851475143?
 * 
 * WIKI eintrag zu Factorization
 * In number theory, the fundamental theorem of arithmetic, 
 * also called the unique factorization theorem or the unique-prime-factorization theorem, 
 * states that every integer greater than 1[3] either is a prime number itself or can be represented as the product of prime numbers and that, 
 * moreover, this representation is unique, up to (except for) the order of the factors.
 */


public class ProjektEuler3 {
    public static void main(String [] args){
        long numm = 600851475143L;
        long temp = numm;
        long largestFact = 0;
         
        int counter = 2;
        while (counter * counter <= temp) {
            if (temp % counter == 0) {
                temp = temp / counter;
                largestFact = counter;
            } else {
                counter++;
            }
        }
        if (temp > largestFact) { // the remainder is a prime number
            largestFact = temp;
        }
        System.out.print(largestFact);
    }
}
